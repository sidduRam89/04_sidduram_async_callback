function fakeAjax(url, cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);

	setTimeout(function () {
		cb(url,fake_responses[url]);
	}, randomDelay);
}

function getFile(file,cb) {
	fakeAjax(file, function(fileName,text){
		render(fileName,text);
	});
	filesWithText[file] = false;
	filesLocation.push(file);
}
let filesWithText = {};
let filesLocation = [];
let index = 0;
// request all filesWithText at once in "parallel"

getFile("file1");
getFile("file2");
getFile("file3");

function render(fileName,text){
	filesWithText[fileName] = text;
	for(;index<filesLocation.length;index++){
		if(filesWithText[filesLocation[index]]){
			console.log(filesWithText[filesLocation[index]]);
			delete filesWithText[filesLocation[index]];
		}else{
			return ;
		}
	}
	console.log("completed..");
}